# Nginx Docker images

## Local build
Alpine:
```bash
docker build . --rm -t nginx-debian:latest -f ./debian/Dockerfile
```

# TODO:
- alpine
- default-magento
- default-phpbb
- add ssl snippets
